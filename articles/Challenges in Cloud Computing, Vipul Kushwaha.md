## Challenges and Risks

*Author: Prof. Vipul Kushwaha*

### 1. Security, Privacy, and Trust

- current cloud offerings are essentially public...exposing the system to more attacks
eg. General Data Protection Regulation

### 2. Data Lock-In and Standardization

- A major concern of cloud computing users is about having their data **locked-in by a certain provider**.
- Users may want to move data and applications out from a provider that does not meet their requirements. However, in their current form, cloud computing infrastructures and platforms **do not employ standard methods of storing user data and applications**.
- They do not interoperate and **user data are not portable**.

### 3. Availability, Fault-Tolerance, and Disaster Recovery

- It is expected that users will have certain expectations about the service level to be provided once their applications are moved to the cloud. 
- These expectations include **availability of the service, its overall performance, and what measures are to be taken when something goes wrong in the system or its components**.

### 4. Resource Management and Energy-Efficiency

- Physical resources such as CPU cores, disk space, and network bandwidth must be sliced and shared among virtual machines running potentially heterogeneous workloads.
- Data centers consumer large amounts of electricity.
- Besides the monetary cost, data centers significantly impact the environment in terms of CO2 emissions from the cooling systems. 
